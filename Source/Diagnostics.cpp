#include "../Include/Diagnostics.hpp"

namespace tnt
{
	namespace diagnostics
	{
		extern "C" __declspec(dllexport) uint32_t  __stdcall   getLastError()
		{
			return 0;
		}

		extern "C" __declspec(dllexport) const char* __stdcall getLastErrorReason()
		{
			return "no error";
		}
	}
}