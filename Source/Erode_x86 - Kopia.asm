.686p                 ;Target processor.  Use instructions for Pentium class machines
;.MODEL FLAT, STDCALL  ;Use the flat memory model. Use stdcall calling conventions
.MODEL FLAT, C  ;Use the flat memory model. Use stdcall calling conventions
option casemap :none
.XMM
.STACK                ;Define a stack segment of 1KB (Not required for this example)
.DATA                 ;Create a near data segment.  Local variables are declared after
                      ;this directive (Not required for this example)
.CODE                 ;Indicates the start of a code segment.

erode3x3crossSSE3Asmx86 PROC ; imgSrc:DWORD, imgDst:DWORD, imageWidth:WORD, imageHeight:WORD
push        ebp  
mov         ebp,esp  
and         esp,0FFFFFFF8h  
sub         esp,10h  
;const uint8_t* srcOffset = imgSrc + (imageWidth * 1) + 1;
;mov         eax,dword ptr [imageWidth]  
;mov         edx,dword ptr [imgSrc]  
movzx       ecx,ax  
inc         edx  
;uint8_t*       dstOffset = imgDst + (imageWidth * 1) + 1;
;const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - ((imageWidth - 1) * 2);
;mov         eax,dword ptr [imageHeight]  
add         edx,ecx  
push        esi  
movzx       esi,ax  
imul        esi,ecx  
lea         eax,[ecx*2-2]  
push        edi  
;CROSS3x3_INIT(0, srcOffset, dstOffset, imageWidth);
lea         edi,[edx-1]  
sub         esi,eax  
lea         eax,[edx+1]  
mov         dword ptr [esp+0Ch],esi  
;mov         esi,dword ptr [imgDst]  
mov         dword ptr [esp+8],eax  
inc         esi  
mov         eax,edx  
add         esi,ecx  
sub         eax,ecx  
add         ecx,edx  
;const int16_t simdOperationsNum = 16;
;for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum )
cmp         dword ptr [esp+0Ch],0  
;jbe         erode3x3crossU8C1_SSE3_2+0B5h (55971875h)  
sub         dword ptr [esp+8],eax  
sub         ecx,eax  
mov         dword ptr [esp+10h],ecx  
sub         esi,eax  
mov         ecx,dword ptr [esp+0Ch]  
sub         edi,eax  
dec         ecx  
mov         dword ptr [esp+14h],esi  
shr         ecx,4  
sub         edx,eax  
inc         ecx  
;jmp         erode3x3crossU8C1_SSE3_2+70h (55971830h)  
lea         esp,[esp]  
;{
;CROSS3x3_ERODE_SSE3(0,x);
mov         esi,dword ptr [esp+8]  
lea         eax,[eax+10h]  
lddqu       xmm0,xmmword ptr [edi+eax-10h]  
lddqu       xmm2,xmmword ptr [eax-10h]  
lddqu       xmm1,xmmword ptr [edx+eax-10h]  
pminub      xmm2,xmm0  
lddqu       xmm0,xmmword ptr [esi+eax-10h]  
mov         esi,dword ptr [esp+10h]  
pminub      xmm1,xmm0  
pminub      xmm2,xmm1  
lddqu       xmm0,xmmword ptr [esi+eax-10h]  
mov         esi,dword ptr [esp+14h]  
pminub      xmm2,xmm0  
movdqu      xmmword ptr [esi+eax-10h],xmm2  
dec         ecx  
;jne         erode3x3crossU8C1_SSE3_2+70h (55971830h)  
;		}
;	}
pop         edi  
pop         esi  
mov         esp,ebp  
pop         ebp  
ret         10h
erode3x3crossSSE3Asmx86 ENDP 
END 

;.586
;;.model flat, stdcall   
'.MODEL FLAT, C    ;Use the flat memory model. Use C calling convention 
;;option casemap :none   

; To get unicode support 
;include		\masm32\macros\ucmacros.asm
  
;include		\masm32\include\kernel32.inc 
;includelib	\masm32\lib\kernel32.lib 
 
;include		\masm32\include\user32.inc 
;includelib	\masm32\lib\user32.lib		

;.data
; WSTR gets you a unicode string definition
;WSTR wstrTitle, "Hello"
