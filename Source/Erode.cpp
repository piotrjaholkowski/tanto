#include "../Include/Tanto/Filters.hpp"
#include "../Include/CrossKernel.hpp"
#include "../Include/RectKernel.hpp"
#include "../Include/EllipseKernel.hpp"

#include "../Include/Dispatcher.hpp"
#include "../Include/CpuInfo.hpp"

#include <stdint.h>
#include <iostream>

#include <emmintrin.h>

namespace tnt
{
	void __stdcall erode5x5ellipseU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	void __stdcall erode5x5ellipseU8C1_AVX2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);

	namespace dispatcher
	{
		void(__stdcall *erode5x5ellipseU8C1)(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight) = nullptr;

		class Dispatch
		{
		public:
			inline Dispatch()
			{
				CpuInfo cpuInfo;
				
				if (cpuInfo.bSSE3Instructions)
				{ 
					erode5x5ellipseU8C1 = tnt::erode5x5ellipseU8C1_SSE3_2;
				}

				if (cpuInfo.bAVX2)
				{
					//erode5x5ellipseU8C1 = tnt::erode5x5ellipseU8C1_AVX2_2;
					erode5x5ellipseU8C1 = tnt::erode5x5ellipseU8C1_SSE3_2;
				}
			}
		};

		Dispatch dispatcherInit;
	}

	extern "C" __declspec(dllexport) void  __stdcall erode3x3crossU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + (imageWidth * 1) + 1;
		uint8_t*       dstOffset = imgDst + (imageWidth * 1) + 1;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - ((imageWidth - 1) * 2);

		CROSS3x3_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 16;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum )
		{
			CROSS3x3_ERODE_SSE3(0,x);
		}
	}


	extern "C" __declspec(dllexport) void  __stdcall erode3x3crossU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + (imageWidth * 1) + 1;
		uint8_t*       dstOffset = imgDst + (imageWidth * 1) + 1;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - ((imageWidth - 1) * 2);

		CROSS3x3_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 32;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			CROSS3x3_ERODE_AVX2(0, x);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + imageWidth + 1;
		uint8_t*       dstOffset = imgDst + imageWidth + 1;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - (2 * imageWidth) - 2;

		RECT3x3_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 16;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			RECT3x3_ERODE_SSE3(0, x);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_SSE3_3(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + imageWidth;
		uint8_t*       dstOffset = imgDst + imageWidth + 1;

		const uint32_t pixelsNum  = ((int32_t)imageWidth * (int32_t)imageHeight) - (2 * imageWidth);
		const uint8_t* offsets[3] = { (srcOffset - imageWidth), srcOffset, (srcOffset + imageWidth) };

		const int16_t simdOperationsNum = 14;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			//_mm_prefetch((const char*)&offsets[0][x + simdOperationsNum], _MM_HINT_T1);
			//_mm_prefetch((const char*)&offsets[1][x + simdOperationsNum], _MM_HINT_T1);
			//_mm_prefetch((const char*)&offsets[2][x + simdOperationsNum], _MM_HINT_T1);

			const __m128i t = _mm_lddqu_si128((const __m128i*)&offsets[0][x]);
			const __m128i m = _mm_lddqu_si128((const __m128i*)&offsets[1][x]);
			const __m128i b = _mm_lddqu_si128((const __m128i*)&offsets[2][x]);

			const __m128i tm     = _mm_min_epu8(t, m);
			const __m128i shift0 = _mm_min_epu8(tm, b);
			const __m128i shift1 = _mm_srli_si128(shift0, 1);
			const __m128i shift2 = _mm_srli_si128(shift0, 2);

			const __m128i minShift01  = _mm_min_epu8(shift0, shift1);
			const __m128i minShift012 = _mm_min_epu8(minShift01, shift2);

			_mm_storeu_si128((__m128i*)&dstOffset[x], minShift012);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode5x5rectU8C1_SSE3(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* offset2   = imgSrc + (imageWidth * 2);
		uint8_t*       dstOffset = imgDst + (imageWidth * 2);

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - (4 * imageWidth);

		const uint8_t* offset0 = offset2 - (2 * imageWidth);
		const uint8_t* offset1 = offset2 - imageWidth;
		const uint8_t* offset3 = offset2 + imageWidth;
		const uint8_t* offset4 = offset2 + (2 * imageWidth);

		const int16_t simdOperationsNum = 12;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			const __m128i o0 = _mm_lddqu_si128((const __m128i*)&offset0[x]);
			const __m128i o1 = _mm_lddqu_si128((const __m128i*)&offset1[x]);
			const __m128i o2 = _mm_lddqu_si128((const __m128i*)&offset2[x]);
			const __m128i o3 = _mm_lddqu_si128((const __m128i*)&offset3[x]);
			const __m128i o4 = _mm_lddqu_si128((const __m128i*)&offset4[x]);

			const __m128i min01  = _mm_min_epu8(o0, o1);
			const __m128i min23  = _mm_min_epu8(o2, o3);
			const __m128i min014 = _mm_min_epu8(min01, o4);
			const __m128i shift0 = _mm_min_epu8(min014, min23);
			

			const __m128i shift1        = _mm_srli_si128(shift0, 1);
			const __m128i minShift01    = _mm_min_epu8(shift0, shift1);
			const __m128i shift2        = _mm_srli_si128(shift0, 2);
			const __m128i shift3        = _mm_srli_si128(shift0, 3);
			const __m128i minShift23    = _mm_min_epu8(shift2, shift3);
			const __m128i shift4        = _mm_srli_si128(shift0, 4);	
			const __m128i minShift014   = _mm_min_epu8(minShift01,  shift4);
			const __m128i minShift01234 = _mm_min_epu8(minShift014, minShift23);

			_mm_storeu_si128((__m128i*)&dstOffset[x], minShift01234);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + imageWidth + 1;
		uint8_t*       dstOffset = imgDst + imageWidth + 1;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - (2 * imageWidth) - 2;

		RECT3x3_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 32;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			RECT3x3_ERODE_AVX2(0, x);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + (imageWidth * 2) + 2;
		uint8_t*       dstOffset = imgDst + (imageWidth * 2) + 2;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - (4 * imageWidth) - 4;

		ELLIPSE5x5_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 16;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			ELLIPSE5x5_ERODE_SSE3(0, x);
		}
	}

	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		const uint8_t* srcOffset = imgSrc + (imageWidth * 2) + 2;
		uint8_t*       dstOffset = imgDst + (imageWidth * 2) + 2;

		const uint32_t pixelsNum = ((int32_t)imageWidth * (int32_t)imageHeight) - (4 * imageWidth) - 4;

		ELLIPSE5x5_INIT(0, srcOffset, dstOffset, imageWidth);

		const int16_t simdOperationsNum = 32;

		for (uint32_t x = 0; x < pixelsNum; x += simdOperationsNum)
		{
			ELLIPSE5x5_ERODE_AVX2(0, x);
		}
	}


	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight)
	{
		//erode3x3crossU8C1_SSE3_2_Test(imgSrc, imgDst, imageWidth, imageHeight);

		CALL_DISPATCHER(erode5x5ellipseU8C1, imgSrc, imgDst, imageWidth, imageHeight);
	}
}

