
.686P
.XMM
include listing.inc
.model	flat

;INCLUDELIB OLDNAMES

PUBLIC	_erode3x3crossU8C1_SSE3_2_Test@16
_TEXT	SEGMENT
_sR0$1$ = -16						; size = 4
_pixelsNum$1$ = -12					; size = 4
_sD0$1$ = -8						; size = 4
_dC0$1$ = -4						; size = 4
_imgSrc$ = 8						; size = 4
_imgDst$ = 12						; size = 4
_imageWidth$ = 16					; size = 2
_imageHeight$ = 20					; size = 2
_erode3x3crossU8C1_SSE3_2_Test@16 PROC	
	push	ebp
	mov	ebp, esp
	and	esp, -8					; fffffff8H
	sub	esp, 16					; 00000010H
	mov	eax, DWORD PTR _imageWidth$[ebp]
	mov	edx, DWORD PTR _imgSrc$[ebp]
	movzx	ecx, ax
	inc	edx
	mov	eax, DWORD PTR _imageHeight$[ebp]
	add	edx, ecx
	push	esi
	movzx	esi, ax
	imul	esi, ecx
	lea	eax, DWORD PTR [ecx*2-2]
	push	edi
	lea	edi, DWORD PTR [edx-1]
	sub	esi, eax
	lea	eax, DWORD PTR [edx+1]
	mov	DWORD PTR _pixelsNum$1$[esp+24], esi
	mov	esi, DWORD PTR _imgDst$[ebp]
	mov	DWORD PTR _sR0$1$[esp+24], eax
	inc	esi
	mov	eax, edx
	add	esi, ecx
	sub	eax, ecx
	add	ecx, edx
	cmp	DWORD PTR _pixelsNum$1$[esp+24], 0
	jbe	SHORT $LN1@erode3x3cr
	sub	DWORD PTR _sR0$1$[esp+24], eax
	sub	ecx, eax
	mov	DWORD PTR _sD0$1$[esp+24], ecx
	sub	esi, eax
	mov	ecx, DWORD PTR _pixelsNum$1$[esp+24]
	sub	edi, eax
	dec	ecx
	mov	DWORD PTR _dC0$1$[esp+24], esi
	shr	ecx, 4
	sub	edx, eax
	inc	ecx
	npad	9
$LL3@erode3x3cr:
	mov	esi, DWORD PTR _sR0$1$[esp+24]
	lea	eax, DWORD PTR [eax+16]
	lddqu	xmm0, XMMWORD PTR [edi+eax-16]
	lddqu	xmm2, XMMWORD PTR [eax-16]
	lddqu	xmm1, XMMWORD PTR [edx+eax-16]
	pminub	xmm2, xmm0
	lddqu	xmm3, XMMWORD PTR [esi+eax-16] ;lddqu	xmm0, XMMWORD PTR [esi+eax-16]
	mov	esi, DWORD PTR _sD0$1$[esp+24]
	pminub	xmm1, xmm3;pminub	xmm1, xmm0
	lddqu	xmm0, XMMWORD PTR [esi+eax-16]
	pminub	xmm2, xmm1
	mov	esi, DWORD PTR _dC0$1$[esp+24]
	pminub	xmm2, xmm0
	movdqu	XMMWORD PTR [esi+eax-16], xmm2
	dec	ecx
	jne	SHORT $LL3@erode3x3cr
$LN1@erode3x3cr:
	pop	edi
	pop	esi
	mov	esp, ebp
	pop	ebp
	ret	16					; 00000010H
_erode3x3crossU8C1_SSE3_2_Test@16 ENDP
_TEXT	ENDS
END
