#include "TimeStamp.hpp"

namespace tnt
{
	uint64_t      TimeStampData::frequency = 1;
	TimeStampData TimeStamp::timeStamps[setup::profiler::timeStampNum];
	uint32_t      TimeStamp::currentTimeStamp = 0;

}