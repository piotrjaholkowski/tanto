#pragma once

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <atomic>


namespace tnt
{

	namespace setup
	{
		namespace profiler
		{
			const int32_t timeStampNum = 1024;
		}
	}

	inline uint64_t QueryPerformance()
	{
		/*uint32_t loword, hiword;

		_asm
		{
		RDTSC
		mov hiword, edx
		mov loword, eax
		}
		return (static_cast<uint64_t>(hiword) << 32) + loword;*/

		int cpuInfo[4];
		__cpuid(cpuInfo, 0); // serialization instruction
		return __rdtsc();
	}

#ifdef USE_PROFILER
#define TEST_TIMESTAMP(Operation) mnd::utils::TimeStamp timeStamp##Operation(Operation)
#else
#define TEST_TIMESTAMP(Operation) 
#endif

	class TimeStampData
	{
	public:
		uint64_t        deltaTime;
		uint64_t        ticks;
		uint8_t         operationType;
		static uint64_t frequency;
	};

	class TimeStamp
	{
	public:
		uint8_t operationType;
		std::chrono::steady_clock::time_point startTime;
		uint64_t             ticks;
		static TimeStampData timeStamps[setup::profiler::timeStampNum];
		static uint32_t      currentTimeStamp;

		inline TimeStamp() : operationType(0)
		{
			std::atomic_thread_fence(std::memory_order_acq_rel);
			startTime = std::chrono::steady_clock::now();
			ticks = QueryPerformance();
		}

		inline TimeStamp(uint8_t opearationType) : operationType(opearationType)
		{
			std::atomic_thread_fence(std::memory_order_acq_rel);
			startTime = std::chrono::steady_clock::now();
			ticks = QueryPerformance();
		}

		inline ~TimeStamp()
		{
			std::atomic_thread_fence(std::memory_order_acq_rel);
			std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
			ticks = QueryPerformance() - ticks;
			timeStamps[currentTimeStamp].deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
			timeStamps[currentTimeStamp].ticks = ticks;
			timeStamps[currentTimeStamp].operationType = operationType;
			currentTimeStamp = ((currentTimeStamp + 1) % setup::profiler::timeStampNum);
		}

		inline static TimeStampData* getTimeStampData(uint8_t operationType) {
			for (uint32_t i = 0; i < currentTimeStamp; ++i) {
				if (timeStamps[i].operationType == operationType) {
					return &timeStamps[i];
				}
			}

			return nullptr;
		}

		inline static int32_t getLastTimeStampId()
		{
			return ((currentTimeStamp - 1) + setup::profiler::timeStampNum) % setup::profiler::timeStampNum;
		}

		inline static uint64_t getMiliseconds()
		{
			int32_t lastTimeStamp = getLastTimeStampId();
			return  timeStamps[lastTimeStamp].deltaTime;
		}

		inline static uint64_t getMicroseconds()
		{
			int32_t lastTimeStamp = getLastTimeStampId();
			return  timeStamps[lastTimeStamp].deltaTime * 1000;
		}

		inline static uint64_t getTicks()
		{
			int32_t lastTimeStamp = getLastTimeStampId();
			return  timeStamps[lastTimeStamp].ticks;
		}

		inline static TimeStampData getLastTimeStamp()
		{
			int32_t lastTimeStamp = getLastTimeStampId();
			return  timeStamps[lastTimeStamp];
		}

		inline static void reset()
		{
			currentTimeStamp = 0;
		}
	};

}


