#include <Tanto/Filters.hpp>
#include <CpuInfo.hpp>

#include <thread>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "TimeStamp.hpp"

int main()
{
	const uint32_t erodeIterations    = 1;
	
	cv::Mat image = cv::imread("jackdaniels.jpg");

	cv::Mat imageGray;

	cv::cvtColor(image, imageGray, cv::COLOR_RGB2GRAY);

	cv::imshow("imageGray", imageGray);

	cv::Mat erodedCross3x3OpenCV;
	cv::Mat erodedRect3x3OpenCV;
	cv::Mat erodedRect5x5OpenCV;
	cv::Mat erodedEllipse5x5OpenCV;
	
	cv::Mat erodedRect3x3AVX2_2Tanto = imageGray.clone();

	cv::Mat erodedRect3x3SSE3_Tanto  = imageGray.clone();
	cv::Mat erodedRect3x3SSE3_2Tanto = imageGray.clone();
	cv::Mat erodedRect3x3SSE3_3Tanto = imageGray.clone();
	cv::Mat erodedRect5x5SSE3_Tanto  = imageGray.clone();

	cv::Mat erodedCross3x3SSE3_2Tanto = imageGray.clone();	
	cv::Mat erodedCross3x3AVX2_2Tanto = imageGray.clone();

	cv::Mat erodedEllipse5x5SE3_2Tanto  = imageGray.clone();
	cv::Mat erodedEllipse5x5AVX2_2Tanto = imageGray.clone();

	cv::Mat kernelCross3x3   = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
	cv::Mat kernelRect3x3    = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
	cv::Mat kernelEllipse5x5 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
	cv::Mat kernelRect5x5    = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));

	std::this_thread::sleep_for(std::chrono::milliseconds(30));

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				cv::erode(imageGray, erodedRect5x5OpenCV, kernelRect5x5);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks cv::erode 5x5 rect" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode5x5rectU8C1_SSE3(imageGray.data, erodedRect5x5SSE3_Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode5x5rectU8C1_SSE3" << std::endl;
	}

	std::cout << std::endl;

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				cv::erode(imageGray, erodedCross3x3OpenCV, kernelCross3x3);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks cv::erode 3x3 cross" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode3x3crossU8C1_SSE3_2(imageGray.data, erodedCross3x3SSE3_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks erode3x3crossU8C1_SSE3_2" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode3x3crossU8C1_AVX2_2(imageGray.data, erodedCross3x3AVX2_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks erode3x3crossU8C1_AVX2_2" << std::endl;
	}

	std::cout << std::endl;

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				cv::erode(imageGray, erodedRect3x3OpenCV, kernelRect3x3);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks cv::erode 3x3 rect" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode3x3rectU8C1_SSE3_2(imageGray.data, erodedRect3x3SSE3_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode3x3rectU8C1_SSE3_2" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode3x3rectU8C1_SSE3_3(imageGray.data, erodedRect3x3SSE3_3Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode3x3rectU8C1_SSE3_3" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode3x3rectU8C1_AVX2_2(imageGray.data, erodedRect3x3AVX2_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode3x3rectU8C1_AVX2_2" << std::endl;
	}

	std::cout << std::endl;
	
	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				cv::erode(imageGray, erodedEllipse5x5OpenCV, kernelEllipse5x5);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks cv::erode 5x5 ellipse" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode5x5ellipseU8C1_SSE3_2(imageGray.data, erodedEllipse5x5SE3_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode5x5ellipseU8C1_SSE3_2" << std::endl;
	}

	{
		tnt::TimeStamp::reset();

		{
			tnt::TimeStamp timeStamp;

			for (uint32_t i = 0; i < erodeIterations; ++i)
			{
				tnt::erode5x5ellipseU8C1_AVX2_2(imageGray.data, erodedEllipse5x5AVX2_2Tanto.data, imageGray.cols, imageGray.rows);
			}
		}

		std::cout << tnt::TimeStamp::getTicks() << " ticks tnt::erode5x5ellipseU8C1_AVX2_2" << std::endl;
	}

    cv::imshow("erodedRect5x5OpenCV", erodedRect5x5OpenCV);
	cv::imshow("erodedRect5x5SSE3_Tanto", erodedRect5x5SSE3_Tanto);

	cv::imshow("erodedEllipse5x5OpenCV", erodedEllipse5x5OpenCV);
	cv::imshow("erodedEllipse5x5SE3_2",  erodedEllipse5x5SE3_2Tanto);
	cv::imshow("erodedEllipse5x5AVX2_2", erodedEllipse5x5AVX2_2Tanto);

	cv::imshow("erodedRect3x3OpenCV",      erodedRect3x3OpenCV);
	cv::imshow("erodedRect3x3SSE3_Tanto",  erodedRect3x3SSE3_Tanto);
	cv::imshow("erodedRect3x3SSE3_2Tanto", erodedRect3x3SSE3_2Tanto);
	cv::imshow("erodedRect3x3SSE3_3Tanto", erodedRect3x3SSE3_3Tanto);
	cv::imshow("erodedRect3x3AVX2_2Tanto", erodedRect3x3AVX2_2Tanto);
	
	cv::imshow("erodedCross3x3OpenCV",       erodedCross3x3OpenCV);
	cv::imshow("erodedCross3x33SSE3_2Tanto", erodedCross3x3SSE3_2Tanto);
	cv::imshow("erodedCross3x3AVX2_2Tanto",  erodedCross3x3AVX2_2Tanto);

	std::cout << std::endl;
	tnt::CpuInfo cpuInfo;
	cpuInfo.printInfo();

	for (;;)
	{
		uint32_t keyPressed = cv::waitKey(1);
	}
}