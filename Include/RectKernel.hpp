#pragma once

#include "pmmintrin.h"
#include "emmintrin.h"
#include "immintrin.h" 

#define RECT3x3_INIT(N,srcOffset,dstOffset,imageWidth)   \
const uint8_t* sCC##N = srcOffset;                       \
uint8_t*       dCC##N = dstOffset;                       \
const uint8_t* sLC##N = srcOffset - 1;                   \
const uint8_t* sRC##N = srcOffset + 1;                   \
const uint8_t* sCT##N = srcOffset - imageWidth;          \
const uint8_t* sLT##N = sCT##N - 1;                      \
const uint8_t* sRT##N = sCT##N + 1;                      \
const uint8_t* sCD##N = srcOffset + imageWidth;          \
const uint8_t* sLD##N = sCD##N - 1;                      \
const uint8_t* sRD##N = sCD##N + 1;

#define RECT3x3_ERODE_SSE3(N,I)                                                       \
const __m128i lt##N                 = _mm_lddqu_si128((const __m128i*)&sLT##N[I]);  \
const __m128i ct##N                 = _mm_lddqu_si128((const __m128i*)&sCT##N[I]);  \
const __m128i ltct##N               = _mm_min_epu8(lt##N, ct##N);                   \
const __m128i rt##N                 = _mm_lddqu_si128((const __m128i*)&sRT##N[I]);  \
const __m128i lc##N                 = _mm_lddqu_si128((const __m128i*)&sLC##N[I]);  \
const __m128i rtlc##N               = _mm_min_epu8(rt##N, lc##N);                   \
const __m128i cc##N                 = _mm_lddqu_si128((const __m128i*)&sCC##N[I]);  \
const __m128i rc##N                 = _mm_lddqu_si128((const __m128i*)&sRC##N[I]);  \
const __m128i ccrc##N               = _mm_min_epu8(rt##N, lc##N);                   \
const __m128i ld##N                 = _mm_lddqu_si128((const __m128i*)&sLD##N[I]);  \
const __m128i cd##N                 = _mm_lddqu_si128((const __m128i*)&sCD##N[I]);  \
const __m128i ldcd##N               = _mm_min_epu8(ld##N, cd##N);                   \
const __m128i ltctrtlc##N           = _mm_min_epu8(ltct##N, rtlc##N);               \
const __m128i ccrcldcd##N           = _mm_min_epu8(ccrc##N, ldcd##N);               \
const __m128i ltctrtlcccrcldcd##N   = _mm_min_epu8(ltctrtlc##N, ccrcldcd##N);       \
const __m128i rd##N                 = _mm_lddqu_si128((const __m128i*)&sRD##N[I]);  \
const __m128i ltctrtlcccrcldcdrd##N = _mm_min_epu8(ltctrtlcccrcldcd##N, rd##N);     \
_mm_storeu_si128((__m128i*)&dCC##N[I], ltctrtlcccrcldcdrd##N);

#define RECT3x3_ERODE_AVX2(N,I)                                                        \
const __m256i lt##N                 = _mm256_lddqu_si256((const __m256i*)&sLT##N[I]);  \
const __m256i ct##N                 = _mm256_lddqu_si256((const __m256i*)&sCT##N[I]);  \
const __m256i ltct##N               = _mm256_min_epu8(lt##N, ct##N);                   \
const __m256i rt##N                 = _mm256_lddqu_si256((const __m256i*)&sRT##N[I]);  \
const __m256i lc##N                 = _mm256_lddqu_si256((const __m256i*)&sLC##N[I]);  \
const __m256i rtlc##N               = _mm256_min_epu8(rt##N, lc##N);                   \
const __m256i cc##N                 = _mm256_lddqu_si256((const __m256i*)&sCC##N[I]);  \
const __m256i rc##N                 = _mm256_lddqu_si256((const __m256i*)&sRC##N[I]);  \
const __m256i ccrc##N               = _mm256_min_epu8(rt##N, lc##N);                   \
const __m256i ld##N                 = _mm256_lddqu_si256((const __m256i*)&sLD##N[I]);  \
const __m256i cd##N                 = _mm256_lddqu_si256((const __m256i*)&sCD##N[I]);  \
const __m256i ldcd##N               = _mm256_min_epu8(ld##N, cd##N);                   \
const __m256i ltctrtlc##N           = _mm256_min_epu8(ltct##N, rtlc##N);               \
const __m256i ccrcldcd##N           = _mm256_min_epu8(ccrc##N, ldcd##N);               \
const __m256i ltctrtlcccrcldcd##N   = _mm256_min_epu8(ltctrtlc##N, ccrcldcd##N);       \
const __m256i rd##N                 = _mm256_lddqu_si256((const __m256i*)&sRD##N[I]);  \
const __m256i ltctrtlcccrcldcdrd##N = _mm256_min_epu8(ltctrtlcccrcldcd##N, rd##N);     \
_mm256_storeu_si256((__m256i*)&dCC##N[I], ltctrtlcccrcldcdrd##N);

#define RECT3x3_SHIFT(N,imageWidth) \
sLT##N += imageWidth;               \
sCT##N += imageWidth;               \
sRT##N += imageWidth;               \
sLC##N += imageWidth;               \
sCC##N += imageWidth;               \
sRC##N += imageWidth;               \
sLD##N += imageWidth;               \
sCD##N += imageWidth;               \
sRD##N += imageWidth;               \
dCC##N += imageWidth;