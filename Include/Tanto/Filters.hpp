#pragma once

#include <stdint.h>

namespace tnt
{
	//asm
	//extern "C" __declspec(dllexport) void  __cdecl erode3x3crossSSE3Asmx86();

	extern "C" __declspec(dllexport) void  __stdcall erode3x3crossU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	
	extern "C" __declspec(dllexport) void  __stdcall erode3x3crossU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);

	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_SSE3_3(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);

	extern "C" __declspec(dllexport) void  __stdcall erode3x3rectU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	extern "C" __declspec(dllexport) void  __stdcall erode5x5rectU8C1_SSE3(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	//
	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1_SSE3_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1_AVX2_2(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
	
	extern "C" __declspec(dllexport) void  __stdcall erode5x5ellipseU8C1(const uint8_t* imgSrc, uint8_t* imgDst, const uint16_t imageWidth, const uint16_t imageHeight);
}