#pragma once

#include "Logs.hpp"

#define CALL_DISPATCHER(Function,...)                                                                                                  \
	if (dispatcher::Function != nullptr)                                                                                               \
			dispatcher::Function(__VA_ARGS__);                                                                                         \
	else                                                                                                                               \
		std::cout << "Error: There is no dispatcher bindings for "<< #Function << " working with your PU architecture" << std::endl;