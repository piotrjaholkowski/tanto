#pragma once

#include "pmmintrin.h"
#include "emmintrin.h"
#include "immintrin.h" 

#define CROSS3x3_INIT(N,srcOffset,dstOffset,imageWidth)  \
const uint8_t* sC##N = srcOffset;                       \
uint8_t*       dC##N = dstOffset;                       \
const uint8_t* sL##N = srcOffset - 1;                   \
const uint8_t* sR##N = srcOffset + 1;                   \
const uint8_t* sT##N = srcOffset - imageWidth;          \
const uint8_t* sD##N = srcOffset + imageWidth;          

#define CROSS3x3_SHIFT(N,imageWidth) \
sT##N += imageWidth;                 \
sL##N += imageWidth;                 \
sC##N += imageWidth;                 \
sR##N += imageWidth;                 \
sD##N += imageWidth;                 \
dC##N += imageWidth;

#define CROSS3x3_ERODE_SSE3(N,I)                                      \
const __m128i t##N     = _mm_lddqu_si128((const __m128i*)&sT##N[I]);  \
const __m128i l##N     = _mm_lddqu_si128((const __m128i*)&sL##N[I]);  \
const __m128i tl##N    = _mm_min_epu8(t##N, l##N);                    \
const __m128i c##N     = _mm_lddqu_si128((const __m128i*)&sC##N[I]);  \
const __m128i r##N     = _mm_lddqu_si128((const __m128i*)&sR##N[I]);  \
const __m128i cr##N    = _mm_min_epu8(c##N, r##N);                    \
const __m128i d##N     = _mm_lddqu_si128((const __m128i*)&sD##N[I]);  \
const __m128i tlcr##N  = _mm_min_epu8(tl##N, cr##N);                  \
const __m128i tlcrd##N = _mm_min_epu8(tlcr##N, d##N);                 \
_mm_storeu_si128((__m128i*)&dC##N[I], tlcrd##N);

#define CROSS3x3_ERODE_AVX2(N,I)                                         \
const __m256i t##N     = _mm256_lddqu_si256((const __m256i*)&sT##N[I]);  \
const __m256i l##N     = _mm256_lddqu_si256((const __m256i*)&sL##N[I]);  \
const __m256i tl##N    = _mm256_min_epu8(t##N, l##N);                    \
const __m256i c##N     = _mm256_lddqu_si256((const __m256i*)&sC##N[I]);  \
const __m256i r##N     = _mm256_lddqu_si256((const __m256i*)&sR##N[I]);  \
const __m256i cr##N    = _mm256_min_epu8(c##N, r##N);                    \
const __m256i d##N     = _mm256_lddqu_si256((const __m256i*)&sD##N[I]);  \
const __m256i tlcr##N  = _mm256_min_epu8(tl##N, cr##N);                  \
const __m256i tlcrd##N = _mm256_min_epu8(tlcr##N, d##N);                 \
_mm256_storeu_si256((__m256i*)&dC##N[I], tlcrd##N);

#define CROSS3x3_DILATATE_SSE3(N)                                     \
const __m128i t##N     = _mm_lddqu_si128((const __m128i*)&sT##N[x]);  \
const __m128i l##N     = _mm_lddqu_si128((const __m128i*)&sL##N[x]);  \
const __m128i tl##N    = _mm_max_epu8(t##N, l##N);                    \
const __m128i c##N     = _mm_lddqu_si128((const __m128i*)&sC##N[x]);  \
const __m128i r##N     = _mm_lddqu_si128((const __m128i*)&sR##N[x]);  \
const __m128i cr##N    = _mm_max_epu8(c##N, r##N);                    \
const __m128i d##N     = _mm_lddqu_si128((const __m128i*)&sD##N[x]);  \
const __m128i tlcr##N  = _mm_max_epu8(tl##N, cr##N);                  \
const __m128i tlcrd##N = _mm_max_epu8(tlcr##N, d##N);                 \
_mm_storeu_si128((__m128i*)&dC##N[x], tlcrd##N);

#define CROSS3x3_DILATATE_AVX2(N)                                        \
const __m256i t##N     = _mm256_lddqu_si256((const __m256i*)&sT##N[x]);  \
const __m256i l##N     = _mm256_lddqu_si256((const __m256i*)&sL##N[x]);  \
const __m256i tl##N    = _mm256_max_epu8(t##N, l##N);                    \
const __m256i c##N     = _mm256_lddqu_si256((const __m256i*)&sC##N[x]);  \
const __m256i r##N     = _mm256_lddqu_si256((const __m256i*)&sR##N[x]);  \
const __m256i cr##N    = _mm256_max_epu8(c##N, r##N);                    \
const __m256i d##N     = _mm256_lddqu_si256((const __m256i*)&sD##N[x]);  \
const __m256i tlcr##N  = _mm256_max_epu8(tl##N, cr##N);                  \
const __m256i tlcrd##N = _mm256_max_epu8(tlcr##N, d##N);                 \
_mm256_storeu_si256((__m256i*)&dC##N[x], tlcrd##N);
                