#pragma once

#include "pmmintrin.h"
#include "emmintrin.h"
#include "immintrin.h" 

#define ELLIPSE5x5_INIT(N,srcOffset,dstOffset,imageWidth)  \
const uint8_t* sI##N = srcOffset;                          \
uint8_t*       dI##N = dstOffset;                          \
const uint8_t* sA##N = srcOffset - (2 * imageWidth);       \
const uint8_t* sD##N = srcOffset - imageWidth;             \
const uint8_t* sN##N = srcOffset + imageWidth;             \
const uint8_t* sR##N = srcOffset + (2 * imageWidth);       \
const uint8_t* sB##N = sD##N - 2;                          \
const uint8_t* sC##N = sD##N - 1;                          \
const uint8_t* sE##N = sD##N + 1;                          \
const uint8_t* sF##N = sD##N + 2;                          \
const uint8_t* sG##N = sI##N - 2;                          \
const uint8_t* sH##N = sI##N - 1;                          \
const uint8_t* sJ##N = sI##N + 1;                          \
const uint8_t* sK##N = sI##N + 2;                          \
const uint8_t* sL##N = sN##N - 2;                          \
const uint8_t* sM##N = sN##N - 1;                          \
const uint8_t* sO##N = sN##N + 1;                          \
const uint8_t* sP##N = sN##N + 2; 

#define ELLIPSE5x5_ERODE_SSE3(N,I)                                                \
const __m128i a##N                 = _mm_lddqu_si128((const __m128i*)&sA##N[I]);  \
const __m128i b##N                 = _mm_lddqu_si128((const __m128i*)&sB##N[I]);  \
const __m128i ab##N                = _mm_min_epu8(a##N, b##N);                    \
const __m128i c##N                 = _mm_lddqu_si128((const __m128i*)&sC##N[I]);  \
const __m128i d##N                 = _mm_lddqu_si128((const __m128i*)&sD##N[I]);  \
const __m128i cd##N                = _mm_min_epu8(c##N, d##N);                    \
const __m128i e##N                 = _mm_lddqu_si128((const __m128i*)&sE##N[I]);  \
const __m128i f##N                 = _mm_lddqu_si128((const __m128i*)&sF##N[I]);  \
const __m128i ef##N                = _mm_min_epu8(e##N, f##N);                    \
const __m128i g##N                 = _mm_lddqu_si128((const __m128i*)&sG##N[I]);  \
const __m128i h##N                 = _mm_lddqu_si128((const __m128i*)&sH##N[I]);  \
const __m128i gh##N                = _mm_min_epu8(g##N, h##N);                    \
const __m128i i##N                 = _mm_lddqu_si128((const __m128i*)&sI##N[I]);  \
const __m128i j##N                 = _mm_lddqu_si128((const __m128i*)&sJ##N[I]);  \
const __m128i ij##N                = _mm_min_epu8(i##N, j##N);                    \
const __m128i k##N                 = _mm_lddqu_si128((const __m128i*)&sK##N[I]);  \
const __m128i l##N                 = _mm_lddqu_si128((const __m128i*)&sL##N[I]);  \
const __m128i kl##N                = _mm_min_epu8(k##N, l##N);                    \
const __m128i m##N                 = _mm_lddqu_si128((const __m128i*)&sM##N[I]);  \
const __m128i n##N                 = _mm_lddqu_si128((const __m128i*)&sN##N[I]);  \
const __m128i mn##N                = _mm_min_epu8(m##N, n##N);                    \
const __m128i o##N                 = _mm_lddqu_si128((const __m128i*)&sO##N[I]);  \
const __m128i p##N                 = _mm_lddqu_si128((const __m128i*)&sP##N[I]);  \
const __m128i op##N                = _mm_min_epu8(o##N, p##N);                    \
const __m128i r##N                 = _mm_lddqu_si128((const __m128i*)&sR##N[I]);  \
const __m128i abcd##N              = _mm_min_epu8(ab##N, cd##N);                  \
const __m128i efgh##N              = _mm_min_epu8(ef##N, gh##N);                  \
const __m128i ijkl##N              = _mm_min_epu8(ij##N, kl##N);                  \
const __m128i mnop##N              = _mm_min_epu8(mn##N, op##N);                  \
const __m128i abcdefgh##N          = _mm_min_epu8(abcd##N, efgh##N);              \
const __m128i ijklmnop##N          = _mm_min_epu8(ijkl##N, mnop##N);              \
const __m128i abcdefghijklmnop##N  = _mm_min_epu8(abcdefgh##N, ijklmnop##N);      \
const __m128i abcdefghijklmnopr##N = _mm_min_epu8(abcdefghijklmnop##N, r##N);     \
_mm_storeu_si128((__m128i*)&dI##N[I], abcdefghijklmnopr##N);

#define ELLIPSE5x5_ERODE_AVX2(N,I)                                                   \
const __m256i a##N                 = _mm256_lddqu_si256((const __m256i*)&sA##N[I]);  \
const __m256i b##N                 = _mm256_lddqu_si256((const __m256i*)&sB##N[I]);  \
const __m256i ab##N                = _mm256_min_epu8(a##N, b##N);                    \
const __m256i c##N                 = _mm256_lddqu_si256((const __m256i*)&sC##N[I]);  \
const __m256i d##N                 = _mm256_lddqu_si256((const __m256i*)&sD##N[I]);  \
const __m256i cd##N                = _mm256_min_epu8(c##N, d##N);                    \
const __m256i e##N                 = _mm256_lddqu_si256((const __m256i*)&sE##N[I]);  \
const __m256i f##N                 = _mm256_lddqu_si256((const __m256i*)&sF##N[I]);  \
const __m256i ef##N                = _mm256_min_epu8(e##N, f##N);                    \
const __m256i g##N                 = _mm256_lddqu_si256((const __m256i*)&sG##N[I]);  \
const __m256i h##N                 = _mm256_lddqu_si256((const __m256i*)&sH##N[I]);  \
const __m256i gh##N                = _mm256_min_epu8(g##N, h##N);                    \
const __m256i i##N                 = _mm256_lddqu_si256((const __m256i*)&sI##N[I]);  \
const __m256i j##N                 = _mm256_lddqu_si256((const __m256i*)&sJ##N[I]);  \
const __m256i ij##N                = _mm256_min_epu8(i##N, j##N);                    \
const __m256i k##N                 = _mm256_lddqu_si256((const __m256i*)&sK##N[I]);  \
const __m256i l##N                 = _mm256_lddqu_si256((const __m256i*)&sL##N[I]);  \
const __m256i kl##N                = _mm256_min_epu8(k##N, l##N);                    \
const __m256i m##N                 = _mm256_lddqu_si256((const __m256i*)&sM##N[I]);  \
const __m256i n##N                 = _mm256_lddqu_si256((const __m256i*)&sN##N[I]);  \
const __m256i mn##N                = _mm256_min_epu8(m##N, n##N);                    \
const __m256i o##N                 = _mm256_lddqu_si256((const __m256i*)&sO##N[I]);  \
const __m256i p##N                 = _mm256_lddqu_si256((const __m256i*)&sP##N[I]);  \
const __m256i op##N                = _mm256_min_epu8(o##N, p##N);                    \
const __m256i r##N                 = _mm256_lddqu_si256((const __m256i*)&sR##N[I]);  \
const __m256i abcd##N              = _mm256_min_epu8(ab##N, cd##N);                  \
const __m256i efgh##N              = _mm256_min_epu8(ef##N, gh##N);                  \
const __m256i ijkl##N              = _mm256_min_epu8(ij##N, kl##N);                  \
const __m256i mnop##N              = _mm256_min_epu8(mn##N, op##N);                  \
const __m256i abcdefgh##N          = _mm256_min_epu8(abcd##N, efgh##N);              \
const __m256i ijklmnop##N          = _mm256_min_epu8(ijkl##N, mnop##N);              \
const __m256i abcdefghijklmnop##N  = _mm256_min_epu8(abcdefgh##N, ijklmnop##N);      \
const __m256i abcdefghijklmnopr##N = _mm256_min_epu8(abcdefghijklmnop##N, r##N);     \
_mm256_storeu_si256((__m256i*)&dI##N[I], abcdefghijklmnopr##N);

#define ELLIPSE5x5_SHIFT(N,imageWidth) \
sA##N += imageWidth;                   \
sB##N += imageWidth;                   \
sC##N += imageWidth;                   \
sD##N += imageWidth;                   \
sE##N += imageWidth;                   \
sF##N += imageWidth;                   \
sG##N += imageWidth;                   \
sH##N += imageWidth;                   \
sI##N += imageWidth;                   \
dI##N += imageWidth;                   \
sJ##N += imageWidth;                   \
sK##N += imageWidth;                   \
sL##N += imageWidth;                   \
sM##N += imageWidth;                   \
sN##N += imageWidth;                   \
sO##N += imageWidth;                   \
sP##N += imageWidth;                   \
sR##N += imageWidth;   