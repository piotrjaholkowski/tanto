#pragma once

#include <stdint.h>

namespace tnt
{
namespace diagnostics
{
	extern "C" __declspec(dllexport) uint32_t  __stdcall   getLastError();
	extern "C" __declspec(dllexport) const char* __stdcall getLastErrorReason();
}
}